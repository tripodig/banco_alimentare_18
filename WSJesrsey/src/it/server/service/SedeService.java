package it.server.service;

import it.server.model.generico.Sede;

import java.io.Serializable;
import java.util.List;

public interface SedeService extends Serializable {

	public abstract List<Sede> findSedeList() throws Exception;
	public abstract Sede findSedeById(Integer idSede) throws Exception;

}