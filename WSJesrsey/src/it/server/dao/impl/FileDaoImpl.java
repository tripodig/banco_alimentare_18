package it.server.dao.impl;

import it.server.dao.FileDao;
import it.server.model.generico.File;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("fileDao")
public class FileDaoImpl implements FileDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<File> findFileList() throws Exception {
		List<File> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(File.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findFileList "+exc.getMessage());
		}
		return results;

	}
	
	public File findFileById(Integer idFile) throws Exception {
		File results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(File.class);
			if(idFile!=null)
				criteria.add(Restrictions.eq("idFile", idFile));
				
			results = (File) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findFileById "+exc.getMessage());
		}
		return results;

	}

	

}

