package it.server.dao.impl;

import it.server.dao.SedeDao;
import it.server.model.generico.Sede;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("sedeDao")
public class SedeDaoImpl implements SedeDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Sede> findSedeList() throws Exception {
		List<Sede> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Sede.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findSedeList "+exc.getMessage());
		}
		return results;

	}
	
	public Sede findSedeById(Integer idSede) throws Exception {
		Sede results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Sede.class);
			if(idSede!=null)
				criteria.add(Restrictions.eq("idSede", idSede));
				
			results = (Sede) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findSedeById "+exc.getMessage());
		}
		return results;

	}

	

}

