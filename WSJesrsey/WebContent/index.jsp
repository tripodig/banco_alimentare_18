<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
   Hello World!
   <br><br>
   Il progetto funziona con Tomcat 7, la jdk 1.7 e db mysql ma � possibile modificare le configurazioni e librerie in relazione alle esigenze
   <br><br>
   PER CHIAMARE IL WS
   <br><br>
   - Inserire nella url:
   <br><br>
   http://localhost:8080/WSJesrsey/rest/userWS/findUtenteList
   <br><br>
   - Selezionare il metodo POST
   <br><br>
   -Inserire Content-Type:  application/json
   <br><br>
   e cliccare su "send"
   <br><br>
   PER CHIAMARE LA SERVLET 
   <br><br>
   - Inserire nella url:
   <br><br>
   http://localhost:8080/WSJesrsey/ServletIndex
   <br><br>
   (Ovviamente se serve la parte web invece delle servlet � possibile integrare JSF o Struts o Spring web o qualsiasi altro framework web)
</body>
</html>