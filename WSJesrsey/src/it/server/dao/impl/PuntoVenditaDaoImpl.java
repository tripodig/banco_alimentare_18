package it.server.dao.impl;

import it.server.dao.PuntoVenditaDao;
import it.server.model.generico.PuntoVendita;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("puntoVenditaDao")
public class PuntoVenditaDaoImpl implements PuntoVenditaDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<PuntoVendita> findPuntoVenditaList() throws Exception {
		List<PuntoVendita> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PuntoVendita.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findPuntoVenditaList "+exc.getMessage());
		}
		return results;

	}
	
	public PuntoVendita findPuntoVenditaById(Integer idPuntoVendita) throws Exception {
		PuntoVendita results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PuntoVendita.class);
			if(idPuntoVendita!=null)
				criteria.add(Restrictions.eq("idPuntoVendita", idPuntoVendita));
				
			results = (PuntoVendita) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findPuntoVenditaById "+exc.getMessage());
		}
		return results;

	}

	

}

