package it.server.wsBean;

public class FileBeanWS implements java.io.Serializable{

	private static final long serialVersionUID = 8421446764462683362L;
	
	private String path;
	private String nomeFile;
	
	private String titolo;
	

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getNomeFile() {
		return nomeFile;
	}
	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}
	

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileBeanWS [path=");
		builder.append(path);
		builder.append(", nomeFile=");
		builder.append(nomeFile);		
		builder.append(", titolo=");
		builder.append(titolo);		
		builder.append("]");
		return builder.toString();
	}

	
}

