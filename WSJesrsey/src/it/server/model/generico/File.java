package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "files")
public class File implements java.io.Serializable{

	private static final long serialVersionUID = -2674299230730459272L;

	private Integer idFile ;
	private Integer idEnte;
	private Integer idUser;
	private String nomeFile;
	private String descrizione;


	public File() {
	}
	
	public File(Integer idFile, Integer idEnte, Integer idUser, String nomeFile, String descrizione) {
		this.idFile = idFile;
		this.idEnte = idEnte;
		this.idUser = idUser;
		this.nomeFile = nomeFile;
		this.descrizione = descrizione;
	}
	
	@Id
    @Column (name = "idFiles")
	public Integer getIdFile() {
		return idFile;
	}
	public void setIdFile(Integer idFile) {
		this.idFile = idFile;
	}
	
    @Column (name = "idEnte")
	public Integer getIdEnte() {
		return idEnte;
	}
	public void setIdEnte(Integer idEnte) {
		this.idEnte = idEnte;
	}
	
    @Column (name = "idUser")
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	
    @Column (name = "nomeFile")
	public String getNomeFile() {
		return nomeFile;
	}
	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}
	
    @Column (name = "descrizione")
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileBean [idFile=");
		builder.append(idFile);
		builder.append(", idEnte=");
		builder.append(idEnte);
		builder.append(", idUser=");
		builder.append(idUser);
		builder.append(", nomeFile=");
		builder.append(nomeFile);
		builder.append(", descrizione=");
		builder.append(descrizione);
		builder.append("]");
		return builder.toString();
	}






}
