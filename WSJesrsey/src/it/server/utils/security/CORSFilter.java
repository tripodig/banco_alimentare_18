package it.server.utils.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CORSFilter implements Filter {

	public void destroy() {}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		// Lets make sure that we are working with HTTP (that is, against HttpServletRequest and HttpServletResponse objects)
		if (req instanceof HttpServletRequest && res instanceof HttpServletResponse) {
		
			HttpServletResponse response = (HttpServletResponse) res;
			HttpServletRequest request = (HttpServletRequest) req;
			
			// Access-Control-Allow-Origin
			response.addHeader("Access-Control-Allow-Origin", "*");
			
			// Access-Control-Max-Age
			response.addHeader("Access-Control-Max-Age", "3600");

			// Access-Control-Allow-Credentials
			response.addHeader("Access-Control-Allow-Credentials", "true");

			// Access-Control-Allow-Methods
			response.addHeader("Access-Control-Allow-Methods", "POST, GET, DELETE,PUT,OPTIONS");

			// Access-Control-Allow-Headers
			response.addHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization, x_auth_token, x_user");
			response.addHeader("Access-Control-Expose-Headers", "x_auth_token, x_user");
			
			response.addHeader("If-Modified-Since", "Mon, 26 Jul 1997 05:00:00 GMT");
			response.addHeader("Cache-Control", "no-cache");
            //OPTIONS: method preflight
			if (!"OPTIONS".equalsIgnoreCase(request.getMethod())) {
				 chain.doFilter(req, res);
			  } 
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

}
