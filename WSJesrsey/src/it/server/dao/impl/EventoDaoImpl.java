package it.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.server.dao.EventoDao;
import it.server.model.generico.*;

@Repository("eventoDao")
public class EventoDaoImpl implements EventoDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Evento> findEventoList() throws Exception {
		List<Evento> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Evento.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findEventoList "+exc.getMessage());
		}
		return results;

	}
	
	public Evento findEventoById(Integer idEvento) throws Exception {
		Evento results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Evento.class);
			if(idEvento!=null)
				criteria.add(Restrictions.eq("idEvento", idEvento));
				
			results = (Evento) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findEventoById "+exc.getMessage());
		}
		return results;

	}

	

}

