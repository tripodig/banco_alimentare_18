package it.server.web;

import it.server.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public abstract class AbstractServlet extends HttpServlet {
	
	private static final long serialVersionUID = 8922318884559667238L;

	@Autowired
	protected UserService userService;
	
	
	public AbstractServlet() {
		super();
	}
	
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
     }
}
