package it.server.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import it.server.model.generico.User;
import it.server.utils.constants.Constants;
import it.server.wsBean.UserBeanWS;

@Path("secure/userWS")
public class UserWS extends AbstractServiceWS{
	
	private static final Logger logger = LoggerFactory.getLogger(UserWS.class);
	
	@GET
	@Path("findUtenteList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response findUtenteList() {
		logger.debug(Constants.METHOD_START);
		
		List<User> list = null;
		List<UserBeanWS> listReturn = null;
		UserBeanWS ws = null;
		String jsonResult = null;
		Gson json = new Gson();
		try {			
			
			listReturn = new ArrayList<UserBeanWS>();
			//list = userService.findUtenteList();
			list = new ArrayList<User>();
			User user = new User();
			user.setIdUser(100);
			user.setUsername("use1");
			user.setAnnoConvenzioni(2001);
			user.setArea("area1");
			user.setCognome("Lagana1");
			user.setNome("Fabio1");
			list.add(user);
			user = new User();
			user.setIdUser(101);
			user.setUsername("use2");
			user.setAnnoConvenzioni(2002);
			user.setArea("area2");
			user.setCognome("Lagana2");
			user.setNome("Fabio2");
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			list.add(user);
			
			for(User ute:list){
				ws = new UserBeanWS();
				ws.setIdUser(ute.getIdUser());
				ws.setNome(ute.getNome());
				ws.setCognome(ute.getCognome());
				ws.setUsername(ute.getUsername());
				ws.setPassword(ute.getPassword());
				ws.setArea(ute.getArea());
				ws.setRuolo(ute.getRuolo());
				ws.setAnnoConvenzioni(ute.getAnnoConvenzioni());
				ws.setProvinciaConvenzioni(ute.getProvinciaConvenzioni());
				listReturn.add(ws);
			}
			
			jsonResult = json.toJson(listReturn);
		}catch(Exception e){
			logger.error(e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return Response.ok(jsonResult).build();
	}
	
	
	
	
}
