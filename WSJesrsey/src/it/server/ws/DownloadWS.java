package it.server.ws;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import it.server.utils.GenericUtility;
import it.server.utils.constants.Constants;
import it.server.utils.exception.GenericException;
import it.server.wsBean.FileBeanWS;

@Path("public/fileWS")
public class DownloadWS extends AbstractServiceWS{
	
	
	private static final Logger logger = LoggerFactory.getLogger(AutenticazioneWS.class);
	
	
	@POST
	@Path("generateFile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response generateFile(FileBeanWS web) {
		
		logger.debug(Constants.METHOD_START);
		
		String jsonResult = null;
		Gson json = new Gson();
		
		try {
			
			logger.debug("Call setLogin Parameter input path: {} , nomeFile: {}", new String[]{web.getPath(), web.getNomeFile()});
			checkRequiredParameters(new String[]{web.getPath(), web.getNomeFile()});
		
			//Generiamo un nome casuale per il file
			String nomeFile = GenericUtility.generateRandom(20)+".pdf";
			
			//Generiamo il file, lo salviamo nella cartella opportuna e salviamo il nome del file sul database
							
			jsonResult = json.toJson(nomeFile);
		}catch(GenericException e){
			logger.error(e.getMessage());
			return Response.status(Status.UNAUTHORIZED).entity(e.toString()).build();
		}catch(Exception e){
			logger.error(e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return Response.ok(jsonResult).build();
	}
	
	
	@POST
	@Path("downloadFile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadFile(@Context HttpServletResponse res, FileBeanWS web) {
		
		logger.debug(Constants.METHOD_START);
		
		OutputStream out;
		
//		String jsonResult = null;
//		Gson json = new Gson();
		
		try {
			web.setNomeFile("fff");
			logger.debug("Call setLogin Parameter input path: {} , nomeFile: {}", new String[]{web.getPath(), web.getNomeFile()});
			checkRequiredParameters(new String[]{web.getPath(), web.getNomeFile()});
			
			out = GenericUtility.downloadFileStream("C:/test/", "test.pdf", "test.pdf", res);
			
		
			//Generiamo un nome casuale per il file
//			String nomeFile = GenericUtility.generateRandom(20)+".pdf";
			
			//Generiamo il file, lo salviamo nella cartella opportuna e salviamo il nome del file sul database
							
//			jsonResult = json.toJson(out);
		}catch(GenericException e){
			logger.error(e.getMessage());
			return Response.status(Status.UNAUTHORIZED).entity(e.toString()).build();
		}catch(Exception e){
			logger.error(e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return Response.ok(out).build();
	}
	
//	@POST
//	@Path("downloadFile")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces("application/pdf")
//	public Response downloadFile(@Context HttpServletResponse res, FileBeanWS web) {
//		
//		logger.debug(Constants.METHOD_START);
//		
//		String jsonResult = null;
//		Gson json = new Gson();
//		OutputStream out;
//		File file;
//		
//		try {
//			
//			logger.debug("Call setLogin Parameter input path: {} , nomeFile: {}", new String[]{web.getPath(), web.getNomeFile()});
//			checkRequiredParameters(new String[]{web.getPath(), web.getNomeFile()});
//			
//			file = new File(web.getPath()+web.getNomeFile());
//			if(file.exists()){
////				out = GenericUtility.downloadFileStream(web.getPath(), web.getNomeFile(), "test_1.pdf", res);
//				jsonResult = Constants.ESITO_OK;
//			}else{
//				logger.error("File not Found");
//				throw new GenericException(Constants.ERROR_500, "Error: File not Found");
//			}	
//							
//			jsonResult = json.toJson(jsonResult);
//		}catch(GenericException e){
//			logger.error(e.getMessage());
//			return Response.status(Status.UNAUTHORIZED).entity(e.toString()).build();
//		}catch(Exception e){
//			logger.error(e.getMessage());
//			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
//		}finally{
//			logger.debug(Constants.METHOD_END);
//		}
//		return Response.ok((Object) file).header("Content-Disposition","attachment; filename=test1.pdf").build();
//	}
//	
//	
//	@Path("doaaaaaaaaaaaawnloadFile1")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_OCTET_STREAM)
//    public Response downloadFile1(){
//		
//		logger.debug(Constants.METHOD_START);
//		
//		StreamingOutput fileStream;
//		
//		try{
//        	fileStream =  new StreamingOutput(){
//	            @Override
//	            public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
//	                    java.nio.file.Path path = Paths.get("C:/test/test5.pdf");
//	                    byte[] data = Files.readAllBytes(path);
//	                    output.write(data);
//	                    output.flush();	
//	            }
//	        };
//		}catch(Exception e){
//			logger.error(e.getMessage());
//			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
//		}finally{
//			logger.debug(Constants.METHOD_END);
//		}
//        return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
//                .header("content-disposition","attachment; filename = myfile.pdf")
//                .build();
//    }
//	
//	@POST
//	@Path("downloadFile")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	private Response downloadFile(FileBeanWS web) {
//		
//		 String fileName = "test.pdf";
//	     Response response = null; 
//	     NumberFormat myFormat = NumberFormat.getInstance(); 
//	     myFormat.setGroupingUsed(true); 
//	       
//	     // Retrieve the file  
//	     File file = new File("C:/test/test.pdf"); 
//	     if (file.exists()) { 
//	       ResponseBuilder builder = Response.ok(file); 
//	       builder.header("Content-Disposition", "attachment; filename=" + file.getName()); 
//	       response = builder.build(); 
//	         
//	       long file_size = file.length(); 
//	             logger.info(String.format("Inside downloadFile==> fileName: %s, fileSize: %s bytes",  
//	                 fileName, myFormat.format(file_size))); 
//	     } else { 
//	       logger.error(String.format("Inside downloadFile==> FILE NOT FOUND: fileName: %s",  
//	                 fileName)); 
//	         
//	       response = Response.status(404). 
//	               entity("FILE NOT FOUND: " + "C:/test/test.pdf"). 
//	               type("text/plain"). 
//	               build(); 
//	     } 
//	        
//	     return response; 
//	}
//	
	
}
