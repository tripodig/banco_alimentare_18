package it.server.service;

import it.server.model.generico.Area;

import java.io.Serializable;
import java.util.List;

public interface AreaService extends Serializable {

	public abstract List<Area> findAreaList() throws Exception;
	public abstract Area findAreaById(Integer idArea) throws Exception;

}
