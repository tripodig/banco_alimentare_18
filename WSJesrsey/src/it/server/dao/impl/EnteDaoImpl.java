package it.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.server.dao.EnteDao;
import it.server.model.generico.*;

@Repository("enteDao")
public class EnteDaoImpl implements EnteDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Ente> findEnteList() throws Exception {
		List<Ente> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Ente.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findEnteList "+exc.getMessage());
		}
		return results;

	}
	
	public Ente findEnteById(Integer idEnte) throws Exception {
		Ente results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Ente.class);
			if(idEnte!=null)
				criteria.add(Restrictions.eq("idEnte", idEnte));
				
			results = (Ente) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findEnteById "+exc.getMessage());
		}
		return results;

	}

	

}

