package it.server.dao.impl;

import it.server.dao.VolontarioDao;
import it.server.model.generico.Volontario;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("volontarioDao")
public class VolontarioDaoImpl implements VolontarioDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Volontario> findVolontarioList() throws Exception {
		List<Volontario> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Volontario.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findVolontarioList "+exc.getMessage());
		}
		return results;

	}
	
	public Volontario findVolontarioById(Integer idVolontario) throws Exception {
		Volontario results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Volontario.class);
			if(idVolontario!=null)
				criteria.add(Restrictions.eq("idVolontario", idVolontario));
				
			results = (Volontario) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findVolontarioById "+exc.getMessage());
		}
		return results;

	}

	

}

