package it.server.utils.exception;


public class GenericException extends Exception{

	private static final long serialVersionUID = -222268354597178862L;
	private int errorCode;
	
	public static final int GENERIC_ERROR = -1;
	
	
	public GenericException(int errorCode,  String message) {
		super(escapeString(message));
		this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return " [Error Code = "+this.getErrorCode()+", Message = "+this.getMessage()+"]";
	}
	
	public static String escapeString(String parameter) {
		return org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(parameter);
	}
}
