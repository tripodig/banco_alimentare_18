package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "ente")
public class Ente implements java.io.Serializable{
	
	private static final long serialVersionUID = 9121990112935152440L;
	
	private Integer idEnte;
    private String nome;
    private String descrizione;
    private String partitaIva;
    private Integer codiceStruttura;
    private Integer codiceSap;
    boolean visibile;

   


	public Ente() {
	}


	public Ente(Integer idEnte, String nome, String descrizione, String partitaIva, Integer codiceStruttura,
			Integer codiceSap, boolean visibile) {
		this.idEnte = idEnte;
		this.nome = nome;
		this.descrizione = descrizione;
		this.partitaIva = partitaIva;
		this.codiceStruttura = codiceStruttura;
		this.codiceSap = codiceSap;
		this.visibile = visibile;
	}

	
	@Id
    @Column (name = "idEnte")
	public Integer getIdEnte() {
		return idEnte;
	}


	public void setIdEnte(Integer idEnte) {
		this.idEnte = idEnte;
	}

	@Column (name = "nome")
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		nome=nome.replace("'", "`");
		this.nome = nome;
	}
	
	@Column (name = "descrizione")
	public String getDescrizione() {
		return this.descrizione;
	}


	public void setDescrizione(String descrizione) {
		if (descrizione!=null)
			descrizione=descrizione.replace("'", "`");
		this.descrizione = descrizione;
	}

	@Column (name = "partitaIva")
	public String getPartitaIva() {
		return partitaIva;
	}


	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ente [idEnte=");
		builder.append(idEnte);
		builder.append(", codiceStruttura=");
		builder.append(codiceStruttura);
		builder.append(", codiceSap=");
		builder.append(codiceSap);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", descrizione=");
		builder.append(descrizione);
		builder.append(", partitaIva=");
		builder.append(partitaIva);
		builder.append("]");
		return builder.toString();
		
	}
	
	@Column (name = "codiceStruttura")
	public Integer getCodiceStruttura() {
		return codiceStruttura;
	}

	public void setCodiceStruttura(Integer codiceStruttura) {
		this.codiceStruttura = codiceStruttura;
	}
	
	@Column (name = "visibile")
	public boolean isVisibile() {
		return visibile;
	}

	public void setVisibile(boolean visibile) {
		this.visibile = visibile;
	}
	
	@Column (name = "codiceSap")
	public Integer getCodiceSap() {
		return codiceSap;
	}
	public void setCodiceSap(Integer codiceSap) {
		this.codiceSap = codiceSap;
	}
 }
