package it.server.ws;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import it.server.model.generico.User;
import it.server.utils.constants.Constants;
import it.server.utils.crypt.JWTUtils;
import it.server.utils.exception.GenericException;
import it.server.wsBean.UserBeanWS;

@Path("public/autenticazioneWS")
public class AutenticazioneWS extends AbstractServiceWS{
	
	
	private static final Logger logger = LoggerFactory.getLogger(AutenticazioneWS.class);
	
	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setLogin(@Context HttpServletResponse res, UserBeanWS userWeb) {
		
		logger.debug(Constants.METHOD_START);
		
		User user = null;
		UserBeanWS userBeanWS = null;
		String jsonResult = null;
		Gson json = new Gson();
		
		try {
			
			MDC.put(Constants.USER, userWeb.getUsername());
			logger.debug("Call setLogin Parameter input username: {} , password: {}", new String[]{userWeb.getUsername(),"********"});
			checkRequiredParameters(new String[]{userWeb.getUsername(),userWeb.getPassword()});
			checkLengthParameters(userWeb.getUsername(), 10);
			checkLengthParameters(userWeb.getPassword(), 10);						
			
//			user = userService.findUtente(userWeb.getUsername(), PasswordUtility.cripta(userWeb.getPassword(), beanProperties.getCriptaKey()));
			user = new User();
			user.setUsername(userWeb.getUsername());
			user.setIdUser(200);
			user.setCognome("Lagan�");
			user.setNome("Fabio");

			if(user!=null){			
				userBeanWS = new UserBeanWS();
				userBeanWS.setUsername(userWeb.getUsername());
				userBeanWS.setIdUser(200);
				setUserToken(res,userWeb.getUsername());
			}else{
				logger.debug("Credenziali Errate Parameter input username: {} , password: {}", new String[]{userWeb.getUsername(),"********"});
				return Response.status(Status.UNAUTHORIZED).build();
			}
			
			jsonResult = json.toJson(userBeanWS);
		}catch(GenericException e){
			logger.error(e.getMessage());
			return Response.status(Status.UNAUTHORIZED).entity(e.toString()).build();
		}catch(Exception e){
			logger.error(e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return Response.ok(jsonResult).build();
	}
	
	
	@POST
	@Path("logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setLogout(@Context HttpServletResponse res) {
		
		logger.debug(Constants.METHOD_START);
		Gson json = new Gson();
		String jsonResult = null;
		try {
			jsonResult = json.toJson("");
			res.addHeader(Constants.X_AUTH_TOKEN, null);
		}catch(Exception e){
			logger.error(e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return Response.ok(jsonResult).build();
	}
	
	
	private void setUserToken(HttpServletResponse res, String username) throws Exception{
		logger.debug(Constants.METHOD_START);
		try{
			String token = JWTUtils.createJWT(username);
			res.addHeader(Constants.X_AUTH_TOKEN, token);
		}catch(Exception e){
			logger.error(e.getMessage());
			throw e;
		}finally{
			logger.debug(Constants.METHOD_END);
		}
	}
	
}
