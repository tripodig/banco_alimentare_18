package it.server.service;

import it.server.model.generico.Persona;

import java.io.Serializable;
import java.util.List;

public interface PersonaService extends Serializable {

	public abstract List<Persona> findPersonaList() throws Exception;
	public abstract Persona findPersonaById(Integer idPersona) throws Exception;

}