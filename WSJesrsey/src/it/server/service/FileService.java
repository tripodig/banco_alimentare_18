package it.server.service;

import it.server.model.generico.File;

import java.io.Serializable;
import java.util.List;

public interface FileService extends Serializable {

	public abstract List<File> findFileList() throws Exception;
	public abstract File findFileById(Integer idFile) throws Exception;

}