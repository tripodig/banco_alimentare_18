package it.server.utils.crypt;


import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.server.utils.constants.Constants;


public class JWTUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(JWTUtils.class);
	
	/**
	 * Il metodo consente di generare un token
	 * @param generic
	 * @return token
	 * @throws Exception 
	 */
	public static String createJWT(String username) throws Exception{
		String id = UUID.randomUUID().toString().replace("-", "");

		Date now = new Date();
		Date exp = new Date(System.currentTimeMillis() + (1000*Constants.EXPIRE_TIME));

		String token = null;
		try {
			token = Jwts.builder()
					.setIssuer(Constants.GENERIC_ISSUER)
					.setSubject(username)
					.setId(id)
					.setIssuedAt(now)
					.setExpiration(exp)
					.signWith(SignatureAlgorithm.HS256, Constants.SECRET)
					.compact();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw e;
		}

		return token;
	}
	
	public static boolean validateToken(String token) throws Exception {
		 boolean validate = false;
		
		 try{
		    Claims claims = Jwts.parser()         
					       .setSigningKey(Constants.SECRET)
					       .parseClaimsJws(token).getBody();
		    
		    Date expiration = claims.getExpiration();
		    logger.debug(expiration.toString());

		    validate = true;
		    
		}catch(Exception e){
			logger.error(e.getMessage());
			validate = false;
		}catch(Throwable e){
			logger.error(e.getMessage());
			validate = false;
		}
		 
		 
		 return validate;
	}
	
	public static String getUsernameJWT(String token) throws Exception {
		 String username = null;		
		 try{
		    Claims claims = Jwts.parser()         
					       .setSigningKey(Constants.SECRET)
					       .parseClaimsJws(token).getBody();
		    
		    username = claims.getSubject();
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw e;
		}
		 return username;
	}
	

	/**
	 * Il metodo seleziona i valori presenti nell'header
	 * 		  
	 * */
	public static Map<String, String> getHeadersInfo(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key.toLowerCase(), value);
        }
        return map;
    }

}
