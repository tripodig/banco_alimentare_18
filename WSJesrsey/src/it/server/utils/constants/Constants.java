package it.server.utils.constants;

public class Constants {
	
	public static final String ESITO_OK = "OK";
	public static final String ESITO_KO = "KO";
	
	public static final String GENERIC_ISSUER = "MyISSUER";
	public static final String SECRET = "myP4$$w0rd";
	
	public static final String METHOD_START = "Start Method";
	public static final String METHOD_END = "End Method";
	
	public static final String TRANSACTION = "TRANSACTION";
	public static final String X_USER = "x_user";
	public static final String USER = "USER";
	public static final String JOB_SISTEMA = "JOB_SISTEMA";
	
	public static final String X_AUTH_TOKEN  = "x_auth_token";	
	
	public static final long EXPIRE_TIME = 3600;//1 ora
	
	public static final int ERROR_401 = 401;
	public static final int ERROR_500 = 500;

}
