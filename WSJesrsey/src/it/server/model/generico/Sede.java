package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "sede")
public class Sede implements java.io.Serializable{
	
	private static final long serialVersionUID = 3118367762760966870L;
	
	private Integer idEnte;
	private Integer idSede;
    private String tipologia;
    private String indirizzo;
    private String comune;
    private String citta;
    private String cap;
    private String provincia;
    private String telefono;
    private String fax;
    private String email;
    
   
	public Sede() {
	}
	
	public Sede(Integer idEnte, Integer idSede, String tipologia, String indirizzo, String comune, String citta,
			String cap, String provincia, String telefono, String fax, String email) {
		this.idEnte = idEnte;
		this.idSede = idSede;
		this.tipologia = tipologia;
		this.indirizzo = indirizzo;
		this.comune = comune;
		this.citta = citta;
		this.cap = cap;
		this.provincia = provincia;
		this.telefono = telefono;
		this.fax = fax;
		this.email = email;
	}

	
    @Column (name = "idEnte")
	public Integer getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(Integer idEnte) {
		this.idEnte = idEnte;
	}
	
	@Id
    @Column (name = "idSede")
	public Integer getIdSede() {
		return idSede;
	}

	public void setIdSede(Integer idSede) {
		this.idSede = idSede;
	}
	
    @Column (name = "tipologia")
	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	
    @Column (name = "indirizzo")
	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
    @Column (name = "comune")
	public String getComune() {
		return comune;
	}

	public void setComune(String comune) {
		this.comune = comune;
	}
	
    @Column (name = "citta")
	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}
	
    @Column (name = "cap")
	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}
	
    @Column (name = "provincia")
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
    @Column (name = "telefono")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
    @Column (name = "fax")
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
    @Column (name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Sede [idEnte=");
		builder.append(idEnte);
		builder.append(", idSede=");
		builder.append(idSede);
		builder.append(", tipologia=");
		builder.append(tipologia);
		builder.append(", indirizzo=");
		builder.append(indirizzo);
		builder.append(", comune=");
		builder.append(comune);
		builder.append(", citta=");
		builder.append(citta);
		builder.append(", cap=");
		builder.append(cap);
		builder.append(", provincia=");
		builder.append(provincia);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", fax=");
		builder.append(fax);
		builder.append(", email=");
		builder.append(email);
		builder.append("]");
		return builder.toString();
	}
	
	

}
   