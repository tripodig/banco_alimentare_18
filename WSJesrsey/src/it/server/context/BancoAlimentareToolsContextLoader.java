package it.server.context;



import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;

import it.server.utils.VersionReader;


public class BancoAlimentareToolsContextLoader extends ContextLoaderListener {
	
	
	private static Logger logger = LoggerFactory.getLogger(BancoAlimentareToolsContextLoader.class);
	
	/**
	 * Initialize the root web application context.
	 */	
	public void contextInitialized(ServletContextEvent event) {
		logger.info("Init Application Context...");
		logger.info("Retrieve Manifest information from file: {}", VersionReader.PATH_MANIFEST+VersionReader.MANIFEST_FILE);
		ServletContext application = event.getServletContext();
		InputStream is = application.getResourceAsStream(VersionReader.PATH_MANIFEST+VersionReader.MANIFEST_FILE);
		VersionReader.setVersion(is);
		super.contextInitialized(event);
	}



	/**
	 * Close the root web application context.
	 */
	public void contextDestroyed(ServletContextEvent event) {
		logger.info("Shutdown Application Context...");
		super.contextDestroyed(event);
	}


	
}

