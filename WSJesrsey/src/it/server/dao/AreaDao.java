package it.server.dao;

import it.server.model.generico.Area;

import java.io.Serializable;
import java.util.List;

public interface AreaDao extends Serializable {
	
	public abstract List<Area> findAreaList() throws Exception;
	public abstract Area findAreaById(Integer idUtente) throws Exception;

}
