package it.server.service.impl;

import it.server.dao.UserDao;
import it.server.model.generico.User;
import it.server.service.UserService;
import it.server.utils.constants.Constants;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("GenericoService")

public class UserServiceImpl implements UserService{
	
	private static final long serialVersionUID = 1L;
	
	static final  Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserDao userDao;	
	
	
	public List<User> findUtenteList() throws Exception {
		logger.debug(Constants.METHOD_START);
		List<User> list = null;
		try{
			list = userDao.findUtenteList();
		}catch(Exception exc){
			logger.error(exc.getMessage());
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}	
		return list;
	}
	
	public User findUtenteById(Integer idUtente) throws Exception {
		logger.debug(Constants.METHOD_START);
		User result = null;
		try{
			result = userDao.findUtenteById(idUtente);
		}catch(Exception exc){
			logger.error(exc.getMessage());
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}	
		return result;
	}
	
	
	public User findUtente(String username, String password) throws Exception {
		logger.debug(Constants.METHOD_START);
		User result = null;
		try{
			result = userDao.findUtente(username, password);
		}catch(Exception exc){
			logger.error(exc.getMessage());
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}	
		return result;
	}
	

	public UserDao getGenericoDao() {return userDao;}
	public void setGenericoDao(UserDao genericoDao) {this.userDao = genericoDao;}


	
}
