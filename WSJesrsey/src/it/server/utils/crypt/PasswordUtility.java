package it.server.utils.crypt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.security.Key;
import java.security.Security;

import javax.crypto.Cipher;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class PasswordUtility {
	
	
	public PasswordUtility() {}
	
	
	/**
	 * Questo metodo permette di criptare una password
	 * */
	public static String cripta(String testo, String path) {
		String base64 = "";
		try {
			Security.addProvider(new com.sun.crypto.provider.SunJCE());
			Cipher cipher = Cipher.getInstance("DESEDE/ECB/PKCS5Padding"); 
			Key key = null;
			Exception e = new Exception();
			e.initCause(null);
			
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(path));
			key = (Key) in.readObject();

			String text = testo;
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] stringBytes = text.getBytes("UTF8");
			byte[] raw = cipher.doFinal(stringBytes);
			BASE64Encoder encoder = new BASE64Encoder();
			base64 = encoder.encode(raw);

		} catch (Exception ex) {
			System.err.println("File contenente la chiave non trovato locale4 "
					+ ex.getMessage());
			System.out.println("errore  " + testo);
			return testo;
		}
		return base64;
	} 

	
	/**
	 * Questo metodo permette di decriptare una password
	 * */
	public static String decripta(String testo, String path) throws Exception {

		Security.addProvider(new com.sun.crypto.provider.SunJCE());

		Cipher cipher = Cipher.getInstance("DESEDE/ECB/PKCS5Padding");

		Key key = null;
		
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					path));

			key = (Key) in.readObject();
			in.close();
		} catch (FileNotFoundException fnfe) {
			System.err.println("File contenente la chiave non trovato - locale5");
			return testo;
			
		}
		
		cipher.init(Cipher.DECRYPT_MODE, key);
		String text = testo;
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] raw = decoder.decodeBuffer(text);
		try {
			byte[] stringBytes = cipher.doFinal(raw);
			String result = new String(stringBytes, "UTF8");
			return result;
		} catch (Exception e) {
			System.err.println("Errore rilevato in sicurezza.java:" + e);
			System.err.flush();
			return null;
		}

	}


}