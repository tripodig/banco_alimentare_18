package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "persona")
public class Persona implements java.io.Serializable{

	private static final long serialVersionUID = -7584516767092086793L;

	private Integer idPersona;
	private Integer idEnte;
	private String titoloReferenza;
	private String nome;
	private String cognome;
	private String indirizzo;
	private String cap;
	private String comune;
	private String citta;
	private String telefono;
	private String cellulare;
	private String email;
	private String cF;
	private String dataNascita;
	private String luogoNascita;

	public Persona() {
	}

	public Persona(Integer idPersona, Integer idEnte, String titoloReferenza, String nome, String cognome,
			String indirizzo, String cap, String comune, String citta, String telefono, String cellulare, String email,
			String cF, String dataNascita, String luogoNascita) {
		super();
		this.idPersona = idPersona;
		this.idEnte = idEnte;
		this.titoloReferenza = titoloReferenza;
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
		this.cap = cap;
		this.comune = comune;
		this.citta = citta;
		this.telefono = telefono;
		this.cellulare = cellulare;
		this.email = email;
		this.cF = cF;
		this.dataNascita = dataNascita;
		this.luogoNascita = luogoNascita;
	}
	
	@Id
    @Column (name = "idPersona")
	public Integer getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}
	
    @Column (name = "idEnte")
	public Integer getIdEnte() {
		return idEnte;
	}
	public void setIdEnte(Integer idEnte) {
		this.idEnte = idEnte;
	}
	
    @Column (name = "titoloReferenza")
	public String getTitoloReferenza() {
		return titoloReferenza;
	}
	public void setTitoloReferenza(String titoloReferenza) {
		this.titoloReferenza = titoloReferenza;
	}
	
    @Column (name = "nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
    @Column (name = "cognome")
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
    @Column (name = "indirizzo")
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
    @Column (name = "cap")
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	
    @Column (name = "comune")
	public String getComune() {
		return comune;
	}
	public void setComune(String comune) {
		this.comune = comune;
	}
	
    @Column (name = "citta")
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
    @Column (name = "telefono")
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
    @Column (name = "cellulare")
	public String getCellulare() {
		return cellulare;
	}
	public void setCellulare(String cellulare) {
		this.cellulare = cellulare;
	}
	
    @Column (name = "Email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
    @Column (name = "CF")
	public String getcF() {
		return cF;
	}
	public void setcF(String cF) {
		this.cF = cF;
	}
	
    @Column (name = "dataNascita")
	public String getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}
	
    @Column (name = "luogoNascita")
	public String getLuogoNascita() {
		return luogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Persona [idPersona=");
		builder.append(idPersona);
		builder.append(", idEnte=");
		builder.append(idEnte);
		builder.append(", titoloReferenza=");
		builder.append(titoloReferenza);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", cognome=");
		builder.append(cognome);
		builder.append(", indirizzo=");
		builder.append(indirizzo);
		builder.append(", cap=");
		builder.append(cap);
		builder.append(", comune=");
		builder.append(comune);
		builder.append(", citta=");
		builder.append(citta);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", cellulare=");
		builder.append(cellulare);
		builder.append(", email=");
		builder.append(email);
		builder.append(", cF=");
		builder.append(cF);
		builder.append(", dataNascita=");
		builder.append(dataNascita);
		builder.append(", luogoNascita=");
		builder.append(luogoNascita);
		builder.append("]");
		return builder.toString();
	}
}
