package it.server.dao;

import it.server.model.generico.Ente;

import java.io.Serializable;
import java.util.List;

public interface EnteDao extends Serializable{
	
	public abstract List<Ente> findEnteList() throws Exception;
	public abstract Ente findEnteById(Integer idEnte) throws Exception;

}
