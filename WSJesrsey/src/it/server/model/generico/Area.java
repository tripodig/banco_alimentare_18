package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "ba_area")
public class Area implements java.io.Serializable{

	private static final long serialVersionUID = 8320429252753280060L;

	private Integer idArea;
	private String area;
	private String regione;
	
	@Id
    @Column (name = "idArea")
	public Integer getIdArea() {
		return idArea;
	}
	public void setIdArea(Integer idArea) {
		this.idArea = idArea;
	}
	
	@Column (name = "area")
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}

	public String getRegione() {
		return regione;
	}
	public void setRegione(String regione) {
		this.regione = regione;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Aree [id=");
		builder.append(idArea);
		builder.append(", area=");
		builder.append(area);
		builder.append(", regione=");
		builder.append(regione);
		builder.append("]");
		return builder.toString();
	}

}
