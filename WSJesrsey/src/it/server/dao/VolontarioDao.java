package it.server.dao;

import it.server.model.generico.Volontario;

import java.io.Serializable;
import java.util.List;

public interface VolontarioDao extends Serializable{
	
	public abstract List<Volontario> findVolontarioList() throws Exception;
	public abstract Volontario findVolontarioById(Integer idVolontario) throws Exception;


}
