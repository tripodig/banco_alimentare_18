package it.server.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.SecureRandom;

public class GenericUtility{
	
	private static final Logger logger = LoggerFactory.getLogger(GenericUtility.class);
	
	java.sql.Date d;
	String patternInput = "yyyy-MM-dd";
	String patternInput1 = "dd/MM/yyyy";
	String patternInput2 = "MM/dd/yyyy HH:mm:ss";
	String patternInput3 = "yyyy/MM/dd";
	String patternOutput = "dd/MM/yyyy";
	String patternOutputOra = "HH:mm:ss";
	String patternOutputDataOra = "dd/MM/yyyy HH:mm:ss";
	String patternOutputSap = "yyyyMMdd";
	String patternAnno = "yyyy";
	Pattern pattern = Pattern.compile("[a-zA-Z0-9_.]+");
	
	public GenericUtility() {}
	
	
	public static Timestamp dataOdiernaTimestamp(){
		java.util.Date today = new java.util.Date();			  
		return(new java.sql.Timestamp(today.getTime()));
	}
	
	public Timestamp dataToTimestamp(java.sql.Date date){		
		 java.sql.Timestamp timeStampDate = new 	Timestamp(date.getTime());
		 return timeStampDate;
	}
	
	public Timestamp dataStringToTimestamp(String date){
		  Date dataDate =stringToDateSql(date.trim());
		 java.sql.Timestamp timeStampDate = new 	Timestamp(dataDate.getTime());
		 return timeStampDate;
	}
	
	public Timestamp dataStringToTimestampFine(String date){
		
		 Date dataDate = stringToDateSql(date.trim());		 
		 GregorianCalendar dn = new GregorianCalendar();
         dn.setTime(dataDate);
         dn.set(Calendar.HOUR_OF_DAY, 23);
         dn.set(Calendar.MINUTE,59);
         dn.set(Calendar.SECOND,59);
         dn.set(Calendar.MILLISECOND, 0);
         java.sql.Timestamp timeStampDate = new Timestamp(dn.getTimeInMillis());
		 return timeStampDate;
		
	}
	
	public Integer dataDaTmsAInteger(Timestamp data){
		
		 String dataS =this.formatta(data);
		 dataS=dataS.substring(6,10)+dataS.substring(3,5)+dataS.substring(0,2);
		 	
		Integer dataI1=new Integer(dataS);
		return dataI1;
	}
	
	
	public int daStringAInt(String data){
		
	
		data=data.substring(6,10)+data.substring(3,5)+data.substring(0,2);
		Integer dataI=new Integer(data);
		
	
		return dataI;
	
	}
	
	public String confrontoDate(String data1,String data2){
		
		data1=data1.substring(6,10)+data1.substring(3,5)+data1.substring(0,2);
		data2=data2.substring(6,10)+data2.substring(3,5)+data2.substring(0,2);
		
		Integer dataI1=new Integer(data1);
		Integer dataI2=new Integer(data2);
		
		
		if (dataI1.compareTo(dataI2)==0)
			return "=";
		else
			if (dataI1.compareTo(dataI2)>0)
				return ">";
			else
				if (dataI1.compareTo(dataI2)<0)
					return "<";
		
		return "";
	
	}
	
	
	
	public static String aaaammgg() {
		Calendar c=Calendar.getInstance();
		String mm="";
		String gg="";
		if((c.get(Calendar.MONTH) + 1) < 10) {
			mm="0" + (c.get(Calendar.MONTH) + 1);
		} else {
			mm="" + (c.get(Calendar.MONTH) + 1);
		}
		if(c.get(Calendar.DAY_OF_MONTH) < 10) {
			gg="0" + c.get(Calendar.DAY_OF_MONTH);
		} else {
			gg="" + c.get(Calendar.DAY_OF_MONTH);

		}
		String Hour="" + c.get(Calendar.HOUR_OF_DAY);
		String minute="" + c.get(Calendar.MINUTE);
		String second="" + c.get(Calendar.SECOND);

		String res=c.get(Calendar.YEAR) + "-" + mm + "-" + gg + "_" + (Hour.length() == 1 ? "0" + Hour : Hour)
				+ (minute.length() == 1 ? "0" + minute : minute) + (second.length() == 1 ? "0" + second : second);
		return res;
	}
	
	public static String annoMeseGiornoCorrente() {
		Calendar c=Calendar.getInstance();
		String mm="";
		String gg="";
		if((c.get(Calendar.MONTH) + 1) < 10) {
			mm="0" + (c.get(Calendar.MONTH) + 1);
		} else {
			mm="" + (c.get(Calendar.MONTH) + 1);
		}
		if(c.get(Calendar.DAY_OF_MONTH) < 10) {
			gg="0" + c.get(Calendar.DAY_OF_MONTH);
		} else {
			gg="" + c.get(Calendar.DAY_OF_MONTH);

		}
		String res=c.get(Calendar.YEAR) + "-" + mm + "-" + gg;
		return res;
	}
	
	public int differenzaInGiorni(Calendar prima, Calendar seconda) {

		long minA = prima.getTimeInMillis() + prima.get(Calendar.ZONE_OFFSET) + prima.get(Calendar.DST_OFFSET);
		long minB = seconda.getTimeInMillis() + seconda.get(Calendar.ZONE_OFFSET) + seconda.get(Calendar.DST_OFFSET);

		int dateDiff = (int) ((minA - minB) / (1000.0 * 60.0 * 60.0 * 24.0));
		return dateDiff;
	}
	 
	
	public int differenzaInGiorni(String prima, String seconda) {
		if(prima==null || seconda==null)
			return -1;
		return differenzaInGiorni(stringToCalendar(prima), stringToCalendar(seconda));

	}
	
	public Calendar stringToCalendar(String data) {
		if (data == null)
			return null;
		Calendar cc = Calendar.getInstance();
		cc.setTime(stringToDateSql(data));
		return cc;
	}
	
	public java.sql.Date stringToDateSql(String data) {
		if (data == null)
			return null;
		SimpleDateFormat sdfIn = new SimpleDateFormat(patternInput);
		try {
			d = new java.sql.Date(sdfIn.parse(data).getTime());
		} catch (Exception e) {
			try {
				sdfIn = new SimpleDateFormat(patternInput1);
				d = new java.sql.Date(sdfIn.parse(data).getTime());
			} catch (Exception e1) {
				try {
					sdfIn = new SimpleDateFormat(patternInput2);
					d = new java.sql.Date(sdfIn.parse(data).getTime());
				} catch (Exception e2) {
					try {
						sdfIn = new SimpleDateFormat(patternInput3);
						d = new java.sql.Date(sdfIn.parse(data).getTime());
					} catch (Exception e3) {
						try {
							sdfIn = new SimpleDateFormat(patternOutput);
							d = new java.sql.Date(sdfIn.parse(data).getTime());
						} catch (Exception e4) {
							try {
								sdfIn = new SimpleDateFormat(patternOutputSap);
								d = new java.sql.Date(sdfIn.parse(data).getTime());
							} catch (Exception e5) {
								System.err.println(e5.toString());
								System.err.println(e4.toString());
								System.err.println(e3.toString());
								System.err.println(e2.toString());
								System.err.println(e1.toString());
								System.err.println(e.toString());
							}
						}
					}
				}
			}
		}
		return d;
	}
	
	
	public String formatta(String data) {
		if (data == null)
			return "";
		SimpleDateFormat sdfIn = new SimpleDateFormat(patternInput);
		SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutput);
		try {
			d = new java.sql.Date(sdfIn.parse(data).getTime());
			return sdfOut.format(d);
		} catch (Exception e) {
			sdfIn = new SimpleDateFormat("d/M/yyyy");
			sdfOut = new SimpleDateFormat(patternOutput);
			try {
				d = new java.sql.Date(sdfIn.parse(data).getTime());
			} catch (ParseException e1) {
				System.out.println(e.toString());
				return data;
			}
			return sdfOut.format(d);

		}
	}
	
	public String formatta(Date d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutput);
			return sdfOut.format(d);

		}
	}
	
	public String formattaOra(Date d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutputOra);
			return sdfOut.format(d);

		}
	}
	
	public String formattaDataOra(Date d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutputDataOra);
			return sdfOut.format(d);

		}
	}
	
	public String formatta(Object d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutput);
			return sdfOut.format(d);

		}
	}
	
	public String formattaOra(Object d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutputOra);
			return sdfOut.format(d);

		}
	}
	
	public String formattaDataOra(Object d) {
		if (d == null)
			return "";
		else {
			SimpleDateFormat sdfOut = new SimpleDateFormat(patternOutputDataOra);
			return sdfOut.format(d);

		}
	}
	
	public String dataPiuGg(String data, String gg){
		if(data==null||data.trim().length()<1)
			return "";
		if(gg==null||gg.trim().length()<1)
			return "";
		Calendar c1= stringToCalendar(data);
		long dataIniziale = c1.getTimeInMillis() + c1.get(Calendar.ZONE_OFFSET) + c1.get(Calendar.DST_OFFSET);
		dataIniziale+=Integer.parseInt(gg)*(1000.0 * 60.0 * 60.0 * 24.0);
		c1.setTimeInMillis(dataIniziale);
		return formatta(c1.getTime());
	} 
	
	public String dataMenoGg(String dataA, String gg){
		if(dataA==null)
			return "";
		if(gg==null||gg.trim().length()<1)
			return "";
		Calendar data= stringToCalendar(dataA);
		long dataIniziale = data.getTimeInMillis() + data.get(Calendar.ZONE_OFFSET) + data.get(Calendar.DST_OFFSET);
		dataIniziale-=Integer.parseInt(gg)*(1000.0 * 60.0 * 60.0 * 24.0);
		data.setTimeInMillis(dataIniziale);
		return formatta(data.getTime());
	} 
	
	 public String DataCorrente(){
	        SimpleDateFormat f = new SimpleDateFormat(patternOutput);
	        return f.format(new Date());   
	    }
	
	/**
	 * Questo metodo elimina i caratteri speciali da una stringa
	 * */
	public  String sistemaCaratteriSpeciali(String string) {
		if (string == null || string == "") {
		      return "";
		    } else {

		        String nomeFileOutput = "";
		        for (int i=0; i<string.length(); i++) {
		                char temp = string.charAt(i);
		                Matcher matcher = pattern.matcher(String.valueOf(temp));
		                if (matcher.matches()){
		                      nomeFileOutput+=String.valueOf(temp);
		                }else{
		                      nomeFileOutput+="_";
		                }
		        }
		        string = nomeFileOutput;
		        string = string.replace("___", "_");
		   }
		    return string;
		  }
	
	
	
	   public String soloNumeriDaStringa(String string){
		      String valore = "";
		      for(int i=0; i<string.length(); i++){
		    	  char b = string.charAt(i);
				  if(!Character.isLetter(b)){
					 valore = valore+b;
				 }
		      }		    	
		      return valore;
	   }
	
	
		public  String sostInverso(String string) {
		    if (string == null) {
		      return "";
		    } else {
		      string = string.replaceAll("�", "&agrave;");
		      string = string.replaceAll("�", "&ograve;");
		      string = string.replaceAll("�", "&igrave;");
		      string = string.replaceAll("�", "&egrave;");
		      string = string.replaceAll("�", "&ugrave;");
		      string = string.replaceAll("�", "&deg;");
		      string = string.replaceAll("�", "&eacute;");
		      string = string.replaceAll("�", "&euro;");
		      string = string.replaceAll("�", "&ccedil;");
		      string = string.replaceAll("�", "&sect;");
		    }
		    return string;
		  }
		
		public  String sost(String string) {
		    if (string == null) {
		      return "";
		    } else {
		      string = string.replaceAll("&agrave;", "�");
		      string = string.replaceAll("&ograve;", "�");
		      string = string.replaceAll("&igrave;", "�");
		      string = string.replaceAll("&egrave;", "�");
		      string = string.replaceAll("&ugrave;", "�");
		      string = string.replaceAll("&deg;", "�");
		      string = string.replaceAll("&eacute;", "�");
		      string = string.replaceAll("&euro;", "�");
		      string = string.replaceAll("&ccedil;", "�");
		      string = string.replaceAll("&sect;", "�");
		      return string;
		    }
		  }
		
		/**
		 * Questo metodo compone una stringa per una IN
		 * */
		public  String componyStringPerIN(List<String> array){
			String result="";
			if(array == null || array.size() == 0)
				return "";

			for(int i=0; i < array.size(); i++) {

				if(i + 1 == array.size())
					result+="'" + array.get(i) + "'";
				else
					result+="'" + array.get(i) + "',";
			}

			return result;
		}
		
		/**
		 * Questo metodo compone una stringa per una IN
		 * */
		public  String componyStringPerIN(HashMap array){
			String result="";
			
			if(array == null || array.size() == 0)
				return "";
			
			List<String> listStr = new ArrayList<String>();
			Set list  = array.keySet();
			Iterator iter = list.iterator();
			while(iter.hasNext()) {
			     Object key = iter.next();
			     listStr.add(key.toString());
			}
            result = componyStringPerIN(listStr);
	            
			return result;
		}
		
		
		public  String componyStringDaIntegerPerIN(List<Integer> array){
			String result="";
			
			if(array == null || array.size() == 0)
				return "";
			
			List<String> listStr = new ArrayList<String>();
			for(Integer value:array){
				listStr.add(value.toString());
			}
            result = componyStringPerIN(listStr);
	            
			return result;
		}
		
		
		
		/**
		 * Questo metodo elimina dei numeri da una stringa
		 * */
		public String eliminaNumeriDaStringa(String string){
			if(string==null)
				return "";
			String valore = "";
			for(int i=0; i<string.length(); i++){
				char b = string.charAt(i);
				if(Character.isLetter(b)){
					valore = valore+b;
				}
			}
			return valore;
		}
		
		
		//Ordinamento decrescente per value 
		public  static Map sortByComparator(Map unsortMap) {
			 
				List list = new LinkedList(unsortMap.entrySet());
		 
				// sort list based on comparator
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						return ((Comparable) ((Map.Entry) (o2)).getValue())
		                                       .compareTo(((Map.Entry) (o1)).getValue());
					}
				});
		 
				// put sorted list into map again
		                //LinkedHashMap make sure order in which keys were inserted
				Map sortedMap = new LinkedHashMap();
				for (Iterator it = list.iterator(); it.hasNext();) {
					Map.Entry entry = (Map.Entry) it.next();
					sortedMap.put(entry.getKey(), entry.getValue());
				}
				return sortedMap;
			}
		
		
		public  Map sortByComparatorKey(Map unsortMap, boolean asc) {
			 
			List list = new LinkedList(unsortMap.entrySet());
	 
			// sort list based on comparator
			if(asc){
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						return ((Comparable) ((Map.Entry) (o1)).getKey())
		                                       .compareTo(((Map.Entry) (o2)).getKey());
					}
				});
			}else{
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						return ((Comparable) ((Map.Entry) (o2)).getKey())
		                                       .compareTo(((Map.Entry) (o1)).getKey());
					}
				});

			}
	 
			// put sorted list into map again
	                //LinkedHashMap make sure order in which keys were inserted
			Map sortedMap = new LinkedHashMap();
			for (Iterator it = list.iterator(); it.hasNext();) {
				Map.Entry entry = (Map.Entry) it.next();
				sortedMap.put(entry.getKey(), entry.getValue());
			}
			return sortedMap;
		}
		
	
		
		public static String generatePassword(String password){

            StringBuilder generatedPassword = new StringBuilder(password.toLowerCase());
            Double n = Math.floor(Math.random() * generatedPassword.length());
            char characterToModify = Character.toUpperCase(generatedPassword.charAt(n.intValue()));
            generatedPassword.setCharAt(n.intValue(), characterToModify);
            int asciiChar = characterToModify;
            generatedPassword.append(asciiChar);
            while (generatedPassword.length() < 8){
                  generatedPassword.append(Math.round(Math.random() * 10));
            }
            
            Random r = new Random(); 
            char randomChar = (char) (97 + r.nextInt(25));
            
            String lettera = String.valueOf(randomChar);
            String letteraRandom = lettera.toUpperCase()+generatedPassword.toString()+lettera.toLowerCase();
            
            return letteraRandom;

		}
		
		public static String sistemaString(String input) throws Exception {
			String output = input;
			try{
				String appo = input.substring(input.length()-1,input.length());
				if(appo.equals("�")||appo.equals("�")||appo.equals("�")||appo.equals("�")||appo.equals("�"))
					output = input.substring(0,input.length()-1);	
				else if(appo.equals("'")&&input.length()>2)
					output = input.substring(0,input.length()-2);
				else if(appo.equals("'"))
					output = input.substring(0,input.length()-1);
				
			} catch (Exception exc) {
				logger.error(exc.getMessage());
				throw new Exception("DAO sistemaString "+exc.getMessage());
			}
			return output;
		}
		
		public static String tokenGenerate(){
			Calendar calendar = Calendar.getInstance();
			String token = UUID.randomUUID().toString().toUpperCase()+"-"+calendar.getTimeInMillis();
			return token;
		}
		
		public static boolean saveFile(File file, String percorso, String nomeFile) throws Exception{
			boolean valid = false;
			try{
				//Verifico se esiste la directory altrimenti la creo
				File fileDirectory = new File(percorso);
				if(!fileDirectory.exists())
					fileDirectory.mkdirs();
				
				File fileDaSalvare = new File(percorso+nomeFile);
				InputStream in = new FileInputStream(file);
				OutputStream out = new FileOutputStream(fileDaSalvare);
			    
				byte[] buf = new byte[1024];
			    int len;
			    while ((len = in.read(buf)) > 0){
				   out.write(buf, 0, len);
			    }
			    in.close();
			    out.close();
			    valid = true;
			}catch(Exception e){
				logger.error(e.getMessage());
				throw new Exception(e.getMessage());
			}
			return valid;
		}
		
		public static ServletOutputStream downloadFile(String path, String nomeFile,String nomeFileNew, HttpServletResponse response) throws Exception {
			ServletOutputStream   out;
			
			try{
				
				File inputFile = new File(path+nomeFile);
				FileInputStream in = new FileInputStream(inputFile);
				out = response.getOutputStream();
				response.setHeader("Pragma", "no-cache");
				
				response.setHeader("Content-disposition", "attachment;filename=\"" + nomeFileNew + "\"");

				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Cache-Control", "must-revalidate");
				response.setHeader("Pragma", "public");

				byte[] content = new byte[1024];
				while (true) {
					int n = in.read(content);
					if (n < 0)
						break;
					out.write(content, 0, n);
				}
				in.close();
				out.flush();
				out.close();
			}catch(Exception e){
				logger.error(e.getMessage());
				throw new Exception(e.getMessage());
			}
			return out;
		}
		
		public static OutputStream downloadFileStream(String path, String nomeFile,String nomeFileNew, HttpServletResponse response) throws Exception {
			OutputStream   out;
			
			try{
				
				File inputFile = new File(path+nomeFile);
				FileInputStream in = new FileInputStream(inputFile);
				out = response.getOutputStream();
				response.setHeader("Pragma", "no-cache");
				
				response.setHeader("Content-disposition", "attachment;filename=\"" + nomeFileNew + "\"");

				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Cache-Control", "must-revalidate");
				response.setHeader("Pragma", "public");

				byte[] content = new byte[1024];
				while (true) {
					int n = in.read(content);
					if (n < 0)
						break;
					out.write(content, 0, n);
				}
				in.close();
				out.flush();
				out.close();
			}catch(Exception e){
				logger.error(e.getMessage());
				throw new Exception(e.getMessage());
			}
			return out;
		}
			 
		public static String generateRandom(int len){
			String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
			SecureRandom rnd = new SecureRandom();
			StringBuilder sb = new StringBuilder( len );
			   for( int i = 0; i < len; i++ ) 
			      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
			   return sb.toString();
		}

}