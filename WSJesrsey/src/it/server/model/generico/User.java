package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "users")
public class User implements java.io.Serializable{

	private static final long serialVersionUID = 8421446764462683362L;
	
	private Integer idUser;
	private String username;
    private String password;
    private String nome;
    private String cognome;
    private String area;
    private String ruolo;
    private Integer annoConvenzioni;
    private String provinciaConvenzioni;
    public boolean valid;
	
    
	
    public User() {
	}

	public User(Integer id, String username, String password, String nome, String cognome, String area,
			String ruolo, Integer annoConvenzioni, String provinciaConvenzioni, boolean valid) {
		this.idUser = id;
		this.username = username;
		this.password = password;
		this.nome = nome;
		this.cognome = cognome;
		this.area = area;
		this.ruolo = ruolo;
		this.annoConvenzioni = annoConvenzioni;
		this.provinciaConvenzioni = provinciaConvenzioni;
		this.valid = valid;
	}
	
	@Id
    @Column (name = "idUsers")
	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer id) {
		this.idUser = id;
	}
	
    @Column (name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
    @Column (name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    @Column (name = "nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
    @Column (name = "cognome")
	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
    @Column (name = "area")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
    @Column (name = "ruolo")
	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
    @Column (name = "annoConvenzioni")
	public Integer getAnnoConvenzioni() {
		return annoConvenzioni;
	}

	public void setAnnoConvenzioni(Integer annoConvenzioni) {
		this.annoConvenzioni = annoConvenzioni;
	}
	
    @Column (name = "provinciaConvenzioni")
	public String getProvinciaConvenzioni() {
		return provinciaConvenzioni;
	}

	public void setProvinciaConvenzioni(String provinciaConvenzioni) {
		this.provinciaConvenzioni = provinciaConvenzioni;
	}
	
    @Column (name = "visibile")
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserBean [id=");
		builder.append(idUser);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", cognome=");
		builder.append(cognome);
		builder.append(", area=");
		builder.append(area);
		builder.append(", ruolo=");
		builder.append(ruolo);
		builder.append(", annoConvenzioni=");
		builder.append(annoConvenzioni);
		builder.append(", provinciaConvenzioni=");
		builder.append(provinciaConvenzioni);
		builder.append(", valid=");
		builder.append(valid);
		builder.append("]");
		return builder.toString();
	}

	
}
