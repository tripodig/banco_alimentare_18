package it.server.web;

import it.server.model.generico.User;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;



public class ServletIndex extends AbstractServlet {
	
	private static final long serialVersionUID = 1L;

	public ServletIndex() {super();}
        
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
     }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		System.out.println("ServletHome --> chiamata GET");
		String forward = "/index.jsp";
		
		try {
			
			List<User> list = userService.findUtenteList();
			for(User ut:list){
				System.out.println(ut.getNome());
			}
			
			RequestDispatcher requestdispatcher=getServletContext().getRequestDispatcher(forward);
			requestdispatcher.forward(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String forward = "/index.jsp";
		
		try {
		
			RequestDispatcher requestdispatcher=getServletContext().getRequestDispatcher(forward);
			requestdispatcher.forward(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
