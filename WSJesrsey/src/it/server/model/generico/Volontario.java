package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "volontari")
public class Volontario implements java.io.Serializable{
	
	private static final long serialVersionUID = 357174834717907293L;
	
	private Integer idVolontario;
    private String nome;
    private String cognome;
    private String indirizzo;
    private String cap;
    private String citta;
    private String telefono;
    private String dataNascita;
    private String occupazione;
    private String email;
    
	public Volontario() {
	}

	public Volontario(Integer idVolontario, String nome, String cognome, String indirizzo, String cap, String citta,
			String telefono, String dataNascita, String occupazione, String email) {
		this.idVolontario = idVolontario;
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
		this.cap = cap;
		this.citta = citta;
		this.telefono = telefono;
		this.dataNascita = dataNascita;
		this.occupazione = occupazione;
		this.email = email;
	}
	
	@Id
    @Column (name = "idVolontario")
	public Integer getIdVolontario() {
		return idVolontario;
	}

	public void setIdVolontario(Integer idVolontario) {
		this.idVolontario = idVolontario;
	}
	
    @Column (name = "nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
    @Column (name = "cognome")
	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
    @Column (name = "indirizzo")
	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
    @Column (name = "cap")
	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}
	
    @Column (name = "citta")
	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}
	
    @Column (name = "telefono")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
    @Column (name = "dataNascita")
	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}
	
    @Column (name = "occupazione")
	public String getOccupazione() {
		return occupazione;
	}

	public void setOccupazione(String occupazione) {
		this.occupazione = occupazione;
	}
	
    @Column (name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Volontario [idVolontario=");
		builder.append(idVolontario);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", cognome=");
		builder.append(cognome);
		builder.append(", indirizzo=");
		builder.append(indirizzo);
		builder.append(", cap=");
		builder.append(cap);
		builder.append(", citta=");
		builder.append(citta);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", dataNascita=");
		builder.append(dataNascita);
		builder.append(", occupazione=");
		builder.append(occupazione);
		builder.append(", email=");
		builder.append(email);
		builder.append("]");
		return builder.toString();
	}
	
    
 }
