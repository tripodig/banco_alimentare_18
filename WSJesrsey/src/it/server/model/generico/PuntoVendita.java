package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "puntovendita")
public class PuntoVendita implements java.io.Serializable{

	private static final long serialVersionUID = -938715440473796694L;

	private Integer idPuntoVendita;
	private String gruppo;
    private String insegna;
    private String provincia;
    private String indirizzo;
    private String cap;
    private String comune;
    private String citta;
    private String telefono;
    private String cellulare;
    private String numCasse;
    private String email;
    private String regione;
    private String area;
    private String mQvendita;
    private String orario;
    private Integer visibile;
    
	@Id
    @Column (name = "idPuntovendita")
	public Integer getIdPuntoVendita() {
		return idPuntoVendita;
	}

	public void setIdPuntoVendita(Integer idPuntoVendita) {
		this.idPuntoVendita = idPuntoVendita;
	}
	
    @Column (name = "gruppo")
	public String getGruppo() {
		return gruppo;
	}

	public void setGruppo(String gruppo) {
		this.gruppo = gruppo;
	}
	
    @Column (name = "insegna")
	public String getInsegna() {
		return insegna;
	}

	public void setInsegna(String insegna) {
		this.insegna = insegna;
	}
	
    @Column (name = "provincia")
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
    @Column (name = "indirizzo")
	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
    @Column (name = "cap")
	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}
	
    @Column (name = "comune")
	public String getComune() {
		return comune;
	}

	public void setComune(String comune) {
		this.comune = comune;
	}
	
    @Column (name = "citta")
	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}
	
    @Column (name = "telefono")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
    @Column (name = "cellulare")
	public String getCellulare() {
		return cellulare;
	}

	public void setCellulare(String cellulare) {
		this.cellulare = cellulare;
	}
	
    @Column (name = "numCasse")
	public String getNumCasse() {
		return numCasse;
	}

	public void setNumCasse(String numCasse) {
		this.numCasse = numCasse;
	}
	
    @Column (name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
    @Column (name = "regione")
	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}
	
    @Column (name = "area")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
    @Column (name = "MQvendita")
	public String getmQvendita() {
		return mQvendita;
	}

	public void setmQvendita(String mQvendita) {
		this.mQvendita = mQvendita;
	}
	
    @Column (name = "orario")
	public String getOrario() {
		return orario;
	}

	public void setOrario(String orario) {
		this.orario = orario;
	}
	
    @Column (name = "visibile")
	public Integer getVisibile() {
		return visibile;
	}

	public void setVisibile(Integer visibile) {
		this.visibile = visibile;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PuntoVendita [idPuntoVendita=");
		builder.append(idPuntoVendita);
		builder.append(", gruppo=");
		builder.append(gruppo);
		builder.append(", insegna=");
		builder.append(insegna);
		builder.append(", provincia=");
		builder.append(provincia);
		builder.append(", indirizzo=");
		builder.append(indirizzo);
		builder.append(", cap=");
		builder.append(cap);
		builder.append(", comune=");
		builder.append(comune);
		builder.append(", citta=");
		builder.append(citta);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", cellulare=");
		builder.append(cellulare);
		builder.append(", numCasse=");
		builder.append(numCasse);
		builder.append(", email=");
		builder.append(email);
		builder.append(", regione=");
		builder.append(regione);
		builder.append(", area=");
		builder.append(area);
		builder.append(", mQvendita=");
		builder.append(mQvendita);
		builder.append(", orario=");
		builder.append(orario);
		builder.append(", visibile=");
		builder.append(visibile);
		builder.append("]");
		return builder.toString();
	}
	
}
