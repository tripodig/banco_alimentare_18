package it.server.model.generico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "evento")
public class Evento implements java.io.Serializable{
	

	private static final long serialVersionUID = -3628996952378442871L;
	
	private Integer idEvento;
    private String nome;
    private String descrizione;
    private String data;
    boolean visibile;
    
    
	public Evento() {

	}
	
	public Evento(Integer idEvento, String nome, String descrizione, String data, boolean visibile) {
		this.idEvento = idEvento;
		this.nome = nome;
		this.descrizione = descrizione;
		this.data = data;
		this.visibile = visibile;
	}
	
	@Id
    @Column (name = "idEvento")
	public Integer getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}
	
    @Column (name = "nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
    @Column (name = "descrizione")
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
    @Column (name = "data")
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
    @Column (name = "visibile")
	public boolean isVisibile() {
		return visibile;
	}
	public void setVisibile(boolean visibile) {
		this.visibile = visibile;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Evento [idEvento=");
		builder.append(idEvento);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", descrizione=");
		builder.append(descrizione);
		builder.append(", data=");
		builder.append(data);
		builder.append(", visibile=");
		builder.append(visibile);
		builder.append("]");
		return builder.toString();
	}

}
