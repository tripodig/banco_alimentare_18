package it.server.dao.impl;

import it.server.dao.PersonaDao;
import it.server.model.generico.Persona;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("personaDao")
public class PersonaDaoImpl implements PersonaDao{

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Persona> findPersonaList() throws Exception {
		List<Persona> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Persona.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findPersonaList "+exc.getMessage());
		}
		return results;

	}
	
	public Persona findPersonaById(Integer idPersona) throws Exception {
		Persona results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Persona.class);
			if(idPersona!=null)
				criteria.add(Restrictions.eq("idPersona", idPersona));
				
			results = (Persona) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findPersonaById "+exc.getMessage());
		}
		return results;

	}

	

}

