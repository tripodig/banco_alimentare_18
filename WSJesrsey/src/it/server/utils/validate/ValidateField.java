package it.server.utils.validate;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateField {
	
	public  boolean obbligatorio =false;
	public  boolean numerico =false;
	public  boolean alfa =false;
	public  boolean numAlfa =false;
	public  boolean lunghezza =false;
	public  boolean email =false;
	public  boolean cf =true;
	public  boolean pi =true;
	public  boolean valorizzato =false;
	public  boolean decimale=false;

	public  void campoObbligatorio(String campo){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
	}
	
	public  void campoObbligatorio(Date campo){
		
		if (campo==null)
			obbligatorio=false;
		else
			obbligatorio=true;
		
	}
	
	public  void campoObbligatorio(Integer campo){
		
		if (campo==null)
			obbligatorio=false;
		else
			obbligatorio=true;
		
	}
	
	public  void campoObbligatorio(Double campo){
		
		if (campo==null)
			obbligatorio=false;
		else
			obbligatorio=true;
		
	}

	public  void campoObbligatorioNumerico(String campo){
			
			if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
				obbligatorio=false;
			else
				obbligatorio=true;
			if (obbligatorio)
			   controllaNumerico(campo);
		
		}
	
	public  void campoObbligatorioNumericonDecimaliLunMax(String campo,int lun){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		  campo =campo.replace(",","");	
		if (obbligatorio)
		   controllaNumerico(campo);
		
		if(campo==null || campo.trim().length()>lun)
			lunghezza=true;
	
	}
	public  void campoObbligatorioNumericonDecimali(Integer campoInt){
		String campo=campoInt.toString();
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		  campo =campo.replace(".","");
		  campo =campo.replace(",","");	
		if (obbligatorio)
		   controllaNumerico(campo);
	
	}
	
	public  void campoObbligatorioNumerico(Integer campo){
		
		if (campo==null || campo.SIZE==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		controllaNumerico(campo.toString());
	
	}
	
	public  void campoNonObbligatorioNumerico(String campo){
		
		if (campo==null || campo.trim().equals("") ||  campo.trim().length()==0){
			valorizzato=false;return;}
			 else { valorizzato=true;}
			
			
		if (campo!=null && !campo.trim().equals("") &&  campo.trim().length()>0)
		   controllaNumerico(campo);
	
	}
	public  void campoNonObbligatorioNumericoConDecimali(String campo){
		
		if (campo==null || campo.trim().equals("") ||  campo.trim().length()==0){
			valorizzato=false;return;}
			 else { valorizzato=true;}
		
		if(campo.contains(".")&&!campo.contains(",")){
			decimale=false;
			return;
		}else{
			decimale=true;
		}
		
		  campo =campo.replace(".","");
		  campo =campo.replace(",","");	
		
		if (campo!=null && !campo.trim().equals("") &&  campo.trim().length()>0)
		   controllaNumerico(campo);
	
	}
	
	public  void campoObbligatorioAlfa(String campo){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		controllaAlfa(campo);	
	}
	public  void campoNonObbligatorioAlfa(String campo){
		if (campo==null || campo.trim().equals("") ||  campo.trim().length()==0){
			valorizzato=false;return;}
			 else { valorizzato=true;}
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()>0)
		    controllaAlfa(campo);	
	}
	public  void campoObbligatorioLunMinAlfa(String campo,int lun){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		controllaAlfa(campo);	
		
		if(campo==null || campo.trim().length()>=lun)
			lunghezza=true;
	}
	public  void campoNonObbligatorioLunFissaAlfa(String campo,int lun){
		
		if (campo==null || campo.trim().equals("") ||  campo.trim().length()==0){
			valorizzato=false;return;}
			 else { valorizzato=true;}
		if (campo!=null && !campo.trim().equals("")|| campo.trim().length()>0){
			controllaAlfa(campo);	
			
			if(campo.trim().length()!=lun && campo.equalsIgnoreCase("rsm"))
				lunghezza=false;
		}
	}
	
	public  void campoObbligatorioLunMin(String campo,int lun){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		if(campo==null || campo.trim().length()>=lun)
			lunghezza=true;
	
	}
	public  void campoObbligatorioLunMinNumerico(String campo,int lun){
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		
		controllaNumerico(campo);
		
		if(campo==null || campo.trim().length()>=lun)
			lunghezza=true;
	}
	public  void campoNOnObbligatorioLunMinNumerico(String campo,int lun){
		
		if (campo==null || campo.trim().equals("") ||  campo.trim().length()==0){
			valorizzato=false;return;}
			 else { valorizzato=true;}
		if (campo!=null && !campo.trim().equals("")&& campo.trim().length()>0){
			controllaNumerico(campo);
			
			if(campo==null || campo.trim().length()>=lun)
				lunghezza=true;
		}
			
	}
	
	public  void campoLunFissa(String campo,int lun){
		
		if (campo.trim().length()!=lun)
			lunghezza=false;
		else
			lunghezza=true;
	}
	public  void campoEmail(String campo){
	
	    String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,11})$";
	    String s[] =campo.split(";");
	    for(int i=0;i<s.length;i++){
	    	if (s[i].trim().matches(emailPattern)){
	    		email= true;
	    	}else{
	    		email= false;
	    		break;
	    	}
	    }
	}
	
	public  boolean campoData(Date data){
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			formatter .setLenient(false) ;
			formatter.parse(data.toString());
		}catch ( Exception exception) { 
			return false;
		}
		return true;
	}
	//Viene fatto solo se nazione selezionata � ITALIA
	public  void controllaCodiceFiscale(String campo){
		
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		if (obbligatorio==false){
			cf=false;
			return;
		}
		//System.out.println("lun cf "+campo.trim().length());
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()==16)
			lunghezza=true;
		if (lunghezza==false){
			cf=false;
			return;
		}
		
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()>0){
			if (numAlfa==false)
					return;
			controllaCF(campo);
			}
	}
	
	//Viene fatto solo se nazione selezionata � ITALIA
	public  void controllaCodiceFiscalePiva(String campo){
		
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		if (obbligatorio==false){
			cf=false;
			return;
		}
		//System.out.println("lun cf "+campo.trim().length());
		if (campo!=null && !campo.trim().equals("") && (campo.trim().length()==16||campo.trim().length()==13))//cf o piva
			lunghezza=true;
		if (lunghezza==false){
			cf=false;
			return;
		}
		
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()>0){
			if (numAlfa==false){
				cf=false;
				return;
			}
			if(campo.trim().length()==16){//cf
				controllaCF(campo);
				if (cf=true) return;
			}
		}
		
		if(!campo.substring(0,2).equals("IT")){
			cf=false;
			return;
		}
		
		   controllaPartitaIva(campo.substring(2,campo.length()));//piva (solo la parte numerica)
		   if (pi==false)
			   cf=false;
	}
	public  void controllaPartitaIva(String campo){
		
		
		if (campo==null || campo.trim().equals("")|| campo.trim().length()==0)
			obbligatorio=false;
		else
			obbligatorio=true;
		if (obbligatorio==false){
			pi=false;
			return;
		}
		
		if (campo!=null && !campo.trim().equals(""))
			controllaNumerico(campo);
		if (numerico==false){
			pi=false;
			return;
		}
		
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()==11)
			lunghezza=true;
		if (lunghezza==false){
			pi=false;
			return;
		}	
		
		if (campo!=null && !campo.trim().equals("") && campo.trim().length()>0){
			controllaPI(campo);
			}
			
	}
	
	
	private void controllaNumerico(String campo) {
		Pattern pattern = Pattern.compile("[0-9]+");
	    //Pattern pattern = Pattern.compile("(?=.*[0-9])");
	    Matcher matcher = pattern.matcher(campo);
	    if (matcher.matches()){
	    	numerico=true;
	    } 
		
	}
	
	public boolean controllaCampoNumerico(String campo) {
		Pattern pattern = Pattern.compile("[0-9]+");
	    //Pattern pattern = Pattern.compile("(?=.*[0-9])");
	    Matcher matcher = pattern.matcher(campo.replaceAll(" ", ""));
	    if (matcher.matches()){
	    	numerico=true;
	    }
	   	return numerico;
	}
	
	public void controllaAlfa(String campo) {
		//Pattern pattern = Pattern.compile("[a-zA-Z]");
		Pattern pattern = Pattern.compile("[A-Z]+");
		campo=campo.replace("�", "a");
		campo=campo.replace("�", "e");
		campo=campo.replace("�", "i");
		campo=campo.replace("�", "o");
		campo=campo.replace("�", "u");
		campo=campo.replace("'", "x");
	    Matcher matcher = pattern.matcher(campo.replaceAll(" ", "").toUpperCase());
	    if (matcher.matches()){
	    	alfa=true;
	    } 
		
	}
	
	private void controllaCF(String campo) {
		Pattern pattern = Pattern.compile("[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}");
	    Matcher matcher = pattern.matcher(campo.toUpperCase());
	    if (matcher.find()){
	    	cf=true;
	    	//return;
	    } 
	    else{
	    	cf=false;
	    }
	    	
	    
	    String set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    String set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    String setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    String setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
	    int s = 0;
	    for( int i = 1; i <= 13; i += 2 ){
	    	 s += setpari.indexOf( set2.charAt( set1.indexOf( campo.toUpperCase().charAt(i) )));
	    }
	       
	    for(int i = 0; i <= 14; i += 2 )
	        s += setdisp.indexOf( set2.charAt( set1.indexOf( campo.toUpperCase().charAt(i) )));
	    int uno=campo.toUpperCase().charAt(15);
	    int due=charCodeAt("A".charAt(0));
	    int tre=s%26;
	    if( s%26 != charCodeAt(campo.toUpperCase().charAt(15))- charCodeAt("A".charAt(0)) )
	    	cf=false;
	}
	private int charCodeAt(char c)
	{ int x; return x = (int) c; } 
		
	
	private void controllaPI(String campo) {
		   int s = 0 ,c=0;
		    for( int i = 0; i <= 9; i += 2 )
		    	s += charCodeAt(campo.charAt(i)) - charCodeAt("0".charAt(0));
		    	
		    for(int  i = 1; i <= 9; i += 2 ){
		        c = 2*( charCodeAt(campo.charAt(i)) - charCodeAt("0".charAt(0)) );
		        if( c > 9 )  c = c - 9;
		        s += c;
		    }
		   
		    //Abbiamo aggiunto come condizione s==0 perch� altrimenti la partita_iva = 00000000000 era valida
		    if(s==0 || ( 10 - s%10 )%10 != charCodeAt(campo.charAt(10)) - charCodeAt("0".charAt(0)) )
		    	pi=false;
	}
	
	public  String controllaDecimali(String campo){
		
		int posVirgo =campo.indexOf(",");
		if (posVirgo>-1)
		    return campo.substring(posVirgo+1);	
		else
			return "";
	}
	public  String controllaInteri(String campo){
		int posVirgo =campo.indexOf(",");
		if (posVirgo>-1)
		   return campo.substring(0,posVirgo);
		else
			return campo;
	}
	
	public  String formattaImporto(String campo,int dec){
		
		campo =campo.trim().replace(".", "");
		int posVirgo =campo.indexOf(",");
		
		String decimali="";
		if (posVirgo>-1)
			decimali=campo.substring(posVirgo+1);
		else
		{
			for (int i=1;i<=dec;i++){
				decimali=decimali+"0";
			}
			
		}
		if (decimali.equals("")){
			for (int i=1;i<=dec;i++){
				decimali=decimali+"0";
			}
		}
			
		String intero="";
		if (posVirgo>-1)
			intero=campo.substring(0,posVirgo);
		else 
			intero=campo;
		String campoReturn="";
		int conta=0;
		//System.out.print("intero.length " +intero.length());
		if (intero.length()>3) {
			for (int i = intero.length();i>0;i--) {
				campoReturn=intero.charAt(i-1)+campoReturn;
				conta++;
				if (conta==3 && i>1){
					conta=0;
					campoReturn="."+campoReturn;
				}
			}
		}else campoReturn=intero;
		
		
		if (dec==0)
			return campoReturn;
		else
		    return campoReturn+","+decimali;	
	}
	
	public boolean controllaPassword(String password){
		
		boolean numerico=false;
		boolean maiuscolo=false;
		boolean minuscolo=false;
		boolean speciale=false;
		
		
		int contaValidi=0;
		for(int i=0;i<password.length();i++){
			char s=password.charAt(i);
			String s1 = Character.toString(s);
			
			boolean numericoP=false;
			boolean maiuscoloP=false;
			boolean minuscoloP=false;
			
			numericoP=controllaNum(s1);
			if(numericoP){ 
				numerico=numericoP;
				continue;
			}
			
			maiuscoloP=controllaMaiuscolo(s1);
			if(maiuscoloP){ 
				maiuscolo=maiuscoloP;
				continue;
			}
			
			minuscoloP=controllaMinuscolo(s1);
			if(minuscoloP){
				minuscolo=minuscoloP;
				continue;
			}
			
			
			speciale=true;
		
			
		}
	
		
		if(numerico)contaValidi=contaValidi+1;
		if(maiuscolo)contaValidi=contaValidi+1;
		
		if(minuscolo)contaValidi=contaValidi+1;
		if(speciale)contaValidi=contaValidi+1;
		
		if(contaValidi<3)
			return false;
		else
			return true;
			
	}
	private   boolean controllaNum(String s) {
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher matcher = pattern.matcher(s);
		if(matcher.matches())return true;
		
		return false;	
	}
	private   boolean controllaMaiuscolo(String s) {
		Pattern pattern = Pattern.compile("[A-Z]+");
		Matcher matcher = pattern.matcher(s);
		if(matcher.matches())return true;
		
		return false;	
	}
	private  boolean controllaMinuscolo(String s) {
		Pattern pattern = Pattern.compile("[a-z]+");
		Matcher matcher = pattern.matcher(s);
		if(matcher.matches())return true;
		
		return false;	
	}
	
	
}