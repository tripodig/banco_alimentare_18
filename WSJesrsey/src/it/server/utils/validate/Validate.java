package it.server.utils.validate;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Validate {
	
	private static final Logger logger = LoggerFactory.getLogger(Validate.class);
	

	private static boolean checkValidCharacters(String field){
		 
		 return (StringUtils.isNotBlank(field) && !field.contains("&") && !field.contains("?"));
	 }
}
