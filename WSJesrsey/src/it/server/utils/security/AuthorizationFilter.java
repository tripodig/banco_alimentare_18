package it.server.utils.security;

import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.server.utils.constants.Constants;
import it.server.utils.crypt.JWTUtils;


public class AuthorizationFilter implements Filter {
	
	private static Logger logger = LoggerFactory.getLogger(AuthorizationFilter.class);
	private String tokenJWT;
	private String token;
	private String usernameHeader;
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {}
	
	@Override
	public void destroy() {}	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain){
		HttpServletRequest req = null;
		HttpServletResponse res = null;
		try {
			
			logger.debug(Constants.METHOD_START);
			
			req = (HttpServletRequest) request;
			res = (HttpServletResponse)response;
			
			Map<String, String> mapHeader = JWTUtils.getHeadersInfo(req);
			
			token = mapHeader.get(Constants.X_AUTH_TOKEN);
			usernameHeader = mapHeader.get(Constants.X_USER);
			
			if(!JWTUtils.validateToken(token)){
				 logger.error("Structure Token is invalid or Token is expired!");
				 unauthorized(res);
				 return;
			}else{			
				String usernameToken = JWTUtils.getUsernameJWT(token);
				if(!StringUtils.isEmpty(usernameToken)&&!StringUtils.isEmpty(usernameHeader)&&StringUtils.equals(usernameHeader, usernameToken)){
					MDC.put(Constants.USER, usernameToken);
					tokenJWT = JWTUtils.createJWT(usernameToken);
					res.addHeader(Constants.X_AUTH_TOKEN, tokenJWT);
				}else{
					 logger.error("Username is invalid!");
					 unauthorized(res);
					 return;
				}
			}			
			clearCache(res);
			chain.doFilter(request, response);
		}catch(Exception e){
			logger.error(e.getMessage());
			unauthorized(res);
			return;
		}finally {
			logger.debug(Constants.METHOD_END);	
		} 
	}
		
	
	//Evita di mantenere i valori in cache
	private void clearCache(HttpServletResponse response){
		if(response!=null){
			response.setHeader("Cache-Control","no-cache"); //Istruzione aggiunta per smemorizzare la cache HTTP 1.1
			response.setHeader("Pragma","no-cache"); //Istruzione aggiunta per smemorizzare la cache HTTP 1.0
			response.setDateHeader ("Expires", 0); //Previene la cache sul proxy server
			response.setHeader("Cache-Control","no-store"); //HTTP 1.1
		}
	}
	
	 private void unauthorized(HttpServletResponse res){
    	logger.error("User not Authorized!");
    	res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
	    
    
	    
}
