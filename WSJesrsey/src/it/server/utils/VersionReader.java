package it.server.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

public class VersionReader {

	private static Logger logger = Logger.getLogger(VersionReader.class.getName());
	public final static String MANIFEST_FILE = "MANIFEST.MF";
	public final static String PATH_MANIFEST = "/META-INF/";
	public final static String PROJECT_VERSION_MDC = "VERSION";
	private final static String VERSION_IN_MANIFEST ="Implementation-Version";
	
	private static String VERSION_CURRENT="UNDEFINED";
	
    private static String implementationFromManifest(InputStream manifestFileIs) {
    	String methodName = "implementationFromManifest()";
    	String value = "UNDEF";
        try {
            Manifest manifest = new Manifest(manifestFileIs);
            Attributes att = manifest.getMainAttributes();
            value = att.getValue(VERSION_IN_MANIFEST);
            if(value==null){
            	value = att.getValue("Manifest-Version");
            }
			logger.info(methodName+"Manifest initialized!");
        } catch (IOException e){
        	logger.error(methodName+"Exception while retrieving Manifest Implementation Version. Error: {}", e);
            logger.error(methodName+"Implementation version is set to UNDEFINED value");
        }
        return value;
    }

    
    public static void setVersion(InputStream is){
    	String implementationVersion = implementationFromManifest(is);
    	VERSION_CURRENT = implementationVersion;    	
    	setVersionInLog();
    }
    
    public static void setVersionInLog(){
    	MDC.put(PROJECT_VERSION_MDC, VERSION_CURRENT);
    }
}
