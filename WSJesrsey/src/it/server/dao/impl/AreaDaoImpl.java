package it.server.dao.impl;

import it.server.dao.AreaDao;
import it.server.model.generico.Area;
import it.server.model.generico.User;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository("areaDao")
public class AreaDaoImpl implements AreaDao {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Area> findAreaList() throws Exception {
		List<Area> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findUtenteList "+exc.getMessage());
		}
		return results;

	}
	
	public Area findAreaById(Integer idArea) throws Exception {
		Area results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
			if(idArea!=null)
				criteria.add(Restrictions.eq("idArea", idArea));
				
			results = (Area) criteria.uniqueResult();
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new Exception("DAO findUtenteById "+exc.getMessage());
		}
		return results;

	}

}
