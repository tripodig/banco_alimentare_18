package it.server.service;

import it.server.model.generico.User;

import java.io.Serializable;
import java.util.List;



public interface UserService extends Serializable{
	
	public abstract List<User> findUtenteList() throws Exception;
	public abstract User findUtenteById(Integer idUtente) throws Exception;
	public abstract User findUtente(String username, String password) throws Exception;
	
}
