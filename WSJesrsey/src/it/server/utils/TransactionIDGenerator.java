package it.server.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TransactionIDGenerator {

	static DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
 
	public static String getNewTransactionId(){
		return String.valueOf(dateFormat.format(new Date())+"-"+UUID.randomUUID());
	}
}
