package it.server.dao;

import java.io.Serializable;
import java.util.List;

import it.server.model.generico.User;


public interface UserDao extends Serializable{
	
	public abstract List<User> findUtenteList() throws Exception;
	public abstract User findUtenteById(Integer idUtente) throws Exception;
	public abstract User findUtente(String username, String password) throws Exception;
	
}