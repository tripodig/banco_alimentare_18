package it.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.server.dao.UserDao;
import it.server.model.generico.User;
import it.server.utils.constants.Constants;

@Repository("userDao")
public class UserDaoImpl implements UserDao{
	
	static final  Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<User> findUtenteList() throws Exception {
		logger.debug(Constants.METHOD_START);
		List<User> results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
			results = criteria.list();
			
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return results;

	}
	
	public User findUtenteById(Integer idUtente) throws Exception {
		logger.debug(Constants.METHOD_START);
		User results = null;

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
			if(idUtente!=null)
				criteria.add(Restrictions.eq("idUsers", idUtente));
				
			results = (User) criteria.uniqueResult();
			
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return results;

	}

	public User findUtente(String username, String password) throws Exception {
		logger.debug(Constants.METHOD_START);
		User user = new User();
		try {	
			user.setUsername("testusername");
			user.setCognome("lagana");
			user.setNome("fabio");
		} catch (Exception exc) {
			logger.error(exc.getMessage());
			throw exc;
		}finally{
			logger.debug(Constants.METHOD_END);
		}
		return user;
	}

	

}
