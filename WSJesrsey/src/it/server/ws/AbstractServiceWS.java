package it.server.ws;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.server.service.UserService;
import it.server.utils.BeanProperties;
import it.server.utils.constants.Constants;
import it.server.utils.exception.GenericException;

public abstract class AbstractServiceWS {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractServiceWS.class);
	
	@Autowired
	protected UserService userService;
	
	@Autowired
	protected BeanProperties beanProperties;
	
	protected void checkRequiredParameters(String[] list) throws Exception {
		for(int i=0;i<list.length;i++){
			if(StringUtils.isBlank(list[i]) ){
				logger.error("Parameter input is null");
				throw new GenericException(Constants.ERROR_401, "Error: Parameter input is null");
			}
		}			
	}
	
	protected void checkLengthParameters(String parameter, int lunghezza) throws Exception {
		if(parameter.length() > lunghezza){
			logger.error("Parameter input is many length");
			throw new GenericException(Constants.ERROR_401, "Parameter input is many length");
		}		
	}
	
	
}