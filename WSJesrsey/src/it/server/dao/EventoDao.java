package it.server.dao;

import it.server.model.generico.Evento;

import java.io.Serializable;
import java.util.List;

public interface EventoDao extends Serializable{
	
	public abstract List<Evento> findEventoList() throws Exception;
	public abstract Evento findEventoById(Integer idEvento) throws Exception;

}
