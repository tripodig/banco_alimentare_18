package it.server.dao;

import it.server.model.generico.PuntoVendita;

import java.io.Serializable;
import java.util.List;

public interface PuntoVenditaDao extends Serializable {

	public abstract List<PuntoVendita> findPuntoVenditaList() throws Exception;
	public abstract PuntoVendita findPuntoVenditaById(Integer idPuntoVendita) throws Exception;

}
