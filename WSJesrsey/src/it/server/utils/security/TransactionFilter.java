package it.server.utils.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.server.utils.TransactionIDGenerator;
import it.server.utils.VersionReader;
import it.server.utils.constants.Constants;

public class TransactionFilter implements Filter {
	
	private static Logger logger = LoggerFactory.getLogger(TransactionFilter.class);
	
	@Override
	public void destroy() {}	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		
		try {			
			
			logger.debug("Init");
			VersionReader.setVersionInLog();						
			MDC.put(Constants.TRANSACTION, TransactionIDGenerator.getNewTransactionId());
			logger.debug(Constants.METHOD_START);
			
			chain.doFilter(request, response);
		}catch(Exception e){
			logger.error(e.getMessage());
		}finally {
			logger.debug(Constants.METHOD_END);
			MDC.remove(Constants.TRANSACTION);
			MDC.remove(Constants.USER);			
		} 
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {}	
	
	
}
